import React from "react";
import { Progress } from "antd";

const ProgressItem = () => {
  return (
    <div className="show_progress">
      <p>
        Marketing <span className="show_red_text">- 140</span>
      </p>
      <Progress percent={50} showInfo={false} />
    </div>
  );
};

export default ProgressItem;
