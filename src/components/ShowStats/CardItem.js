import React from "react";

const CardItem = () => {
  return (
    <div className="col-md-6 col-lg-6 col-12">
      <div className="show_number_card">
        <span className="show_margin_bottom_1 show_big_text show_red_text">
          2100
        </span>
        <span className="show_small_text">New users</span>
      </div>
    </div>
  );
};

export default CardItem;
