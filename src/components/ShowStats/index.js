import React from "react";
import "./ShowStats.scss";
import CardItem from "./CardItem";
import ProgressItem from "./ProgressItem";

const ShowStats = () => {
  return (
    <div className="show_stats">
      <div className="show_margin_1">
        <div className="header">
          <span className="show_big_text show_600_weight">Stats</span>
        </div>
        <div className="show_stats_container show_margin_2_top">
          <div className="container">
            <div className="row">
              {[...Array(4)].map((_, index) => (
                <CardItem key={index} />
              ))}
            </div>
          </div>
        </div>
        <div className="header">
          <span className="show_big_text show_600_weight">
            Followers <br /> interests
          </span>
        </div>
        <div className="show_stats_progress show_margin_bottom_1">
          {[...Array(5)].map((_, index) => (
            <ProgressItem key={index} />
          ))}
        </div>
        <div className="show_update_prem show_cursored">
          <h6 className="show_medium_text show_uppercased_text show_600_weight show_red_text">
            Update to premium{" "}
          </h6>
        </div>
      </div>
    </div>
  );
};

export default ShowStats;
