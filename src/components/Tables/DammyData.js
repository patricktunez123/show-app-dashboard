export const Attendees = [
  {
    key: "1",
    name: "Yves Honore",
    email: "name@email.domain",
    attendence: true,
  },
  {
    key: "2",
    name: "Yves Honore",
    email: "name@email.domain",
    attendence: true,
  },
  {
    key: "3",
    name: "Yves Honore",
    email: "name@email.domain",
    attendence: true,
  },
  {
    key: "4",
    name: "Yves Honore",
    email: "name@email.domain",
    attendence: true,
  },
  {
    key: "5",
    name: "Yves Honore",
    email: "name@email.domain",
    attendence: true,
  },
  {
    key: "6",
    name: "Yves Honore",
    email: "name@email.domain",
    attendence: true,
  },
  {
    key: "7",
    name: "Yves Honore",
    email: "name@email.domain",
    attendence: true,
  },
  {
    key: "8",
    name: "Yves Honore",
    email: "name@email.domain",
    attendence: false,
  },
  {
    key: "9",
    name: "Yves Honore",
    email: "name@email.domain",
    attendence: false,
  },
  {
    key: "10",
    name: "Yves Honore",
    email: "name@email.domain",
    attendence: false,
  },
];
