import React from "react";
import { Table, Popconfirm, Checkbox } from "antd";
import { FaUserCircle } from "react-icons/fa";
import { AiOutlineClose, AiOutlineExclamationCircle } from "react-icons/ai";
import { Attendees } from "./DammyData";

const AttendeesTable = () => {
  const columns = [
    {
      title: <span className="">#</span>,
      dataIndex: "key",
      key: "key",
    },
    {
      title: <span className="">Name</span>,
      dataIndex: "name",
      key: "name",
      render: (_, record) => (
        <div className="show_flex">
          <FaUserCircle className="show_big_text" />
          <span className="show_600_weight">{record?.name}</span>
        </div>
      ),
    },
    {
      title: <span className="">Email</span>,
      dataIndex: "email",
      key: "email",
      render: (_, record) => (
        <span className="show_mute_text show_underlined_text">
          {record?.email}
        </span>
      ),
    },
    {
      title: <span className="">Attendence</span>,
      dataIndex: "attendence",
      key: "attendence",
      render: (_, record) => (
        <div>
          {record?.attendence ? (
            <Checkbox defaultChecked></Checkbox>
          ) : (
            <div className="show_flex">
              <Checkbox></Checkbox> <span>Record</span>
            </div>
          )}
        </div>
      ),
    },
    {
      title: "Action",
      dataIndex: "action",
      render: () => (
        <div className="action--buttons">
          <Popconfirm
            title="Are you sure?"
            okText="Some action"
            cancelText="Cancel"
          >
            <AiOutlineExclamationCircle className="show_cursored show_text_18 show_mute_text" />
          </Popconfirm>
          <Popconfirm
            title="Are you sure to delete this?"
            okText="Delete"
            cancelText="Cancel"
          >
            <AiOutlineClose className="show_cursored show_text_18 show_mute_text" />
          </Popconfirm>
        </div>
      ),
    },
  ];

  return <Table columns={columns} dataSource={Attendees} pagination={false} />;
};

export default AttendeesTable;
