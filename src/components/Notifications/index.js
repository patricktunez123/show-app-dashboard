import React from "react";
import { Drawer } from "antd";
import { AiOutlineClose } from "react-icons/ai";
import "./Notifications.scss";
import Notification from "./Notification";

const index = ({ onClose, visible }) => {
  return (
    <Drawer
      title="My activites"
      width={500}
      placement="right"
      onClose={onClose}
      visible={visible}
      // mask={false}
      headerStyle={{ display: "none" }}
      className="notifications_drawer"
    >
      <div className="notifications">
        <div className="notifications_header">
          <h6 className="show_big_text show_white_text show_600_weight">
            My activites
          </h6>
          <AiOutlineClose
            onClick={onClose}
            className="show_big_text show_white_text show_600_weight show_cursored"
          />
        </div>
        <div className="notifications_subheader">
          <p className="show_white_text">Filter activities by</p>
          <p className="show_white_text show_600_weight">Today</p>
        </div>
        <div className="notifications_body">
          {[...Array(15)].map((_, index) => (
            <Notification key={index} />
          ))}
        </div>
      </div>
    </Drawer>
  );
};

export default index;
