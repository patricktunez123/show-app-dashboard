import React from "react";
import { FaUserCircle } from "react-icons/fa";
import { MdNavigateNext } from "react-icons/md";

const Notification = () => {
  return (
    <div className="notification">
      <div className="profile_pic_container">
        <FaUserCircle className="profile_pic" />
      </div>
      <div className="notification_details">
        <p>
          Yves Honore <span className="show_mute_text">and</span> 245{" "}
          <span className="show_mute_text">
            others registered on your event
          </span>{" "}
          Africa Young Techies Summit 2020.
        </p>
        <p className="show_mute_text show_small_text">14 Feb 2020</p>
      </div>
      <MdNavigateNext className="show_extra_big_text show_mute_text" />
    </div>
  );
};

export default Notification;
