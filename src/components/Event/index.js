import React, { useState } from "react";
import { Card, Drawer, Button } from "antd";
import { AiOutlinePause, AiOutlineClose } from "react-icons/ai";
import { FaLongArrowAltRight } from "react-icons/fa";
import { MdModeEdit, MdDelete } from "react-icons/md";
import eventBg from "../../images/event_bg.png";
import "./Event.scss";
import EventMetrics from "./EventMetrics";
import EventDetails from "./EventDetails";
import Attendance from "./Attendance";

const { Meta } = Card;

const Event = ({
  showModal,
  onEventMetrics,
  onEventAttendees,
  selectedMenu,
}) => {
  const [visible, setVisible] = useState(false);
  const viewEventDrawer = () => {
    setVisible(true);
  };

  const onClose = () => {
    setVisible(false);
  };

  return (
    <div className="col-md-3 col-lg-3 col-12">
      <Card
        cover={<img alt="" src={eventBg} />}
        actions={[
          <span className="show_extra_small_text">ONGOING</span>,
          <AiOutlinePause key="pause" />,
          <AiOutlineClose key="cancel" />,
          <FaLongArrowAltRight onClick={viewEventDrawer} key="ellipsis" />,
        ]}
      >
        <Meta
          title="EAst frica young techies 2020"
          description="14 February 2020"
        />
      </Card>
      <Drawer
        width="91.7%"
        title="Event"
        placement="right"
        closable={true}
        onClose={onClose}
        visible={visible}
        mask={false}
        headerStyle={{ display: "none" }}
        className="show_event_drawer"
      >
        <div className="show_event_container">
          <div className="show_event_drawer_left">
            <EventDetails />
          </div>
          <div className="show_event_drawer_right">
            <div className="show_event_header">
              <span
                className={`show_event_header_menu show_cursored ${
                  selectedMenu === "EVENTMETRICS"
                    ? "show_red_text show_600_weight"
                    : "show_grey_text"
                } `}
                onClick={onEventMetrics}
              >
                EVENT METRICS
              </span>
              <span
                className={`show_event_header_menu show_cursored ${
                  selectedMenu === "ATTENDEES"
                    ? "show_red_text show_600_weight"
                    : "show_grey_text"
                } `}
                onClick={onEventAttendees}
              >
                ATTENDEES
              </span>
              <Button
                type="link"
                icon={<MdModeEdit />}
                className="show_link_btn show_event_header_menu show_12_btn"
                onClick={showModal}
              >
                Edit event
              </Button>
              <Button
                type="link"
                icon={<AiOutlinePause />}
                className="show_link_btn show_event_header_menu show_12_btn"
              >
                Pause
              </Button>
              <Button
                type="link"
                icon={<MdDelete />}
                className="show_link_btn show_red_btn show_12_btn"
              >
                Delete
              </Button>
              <Button
                type="link"
                icon={<AiOutlineClose />}
                className="show_link_btn show_black_btn show_12_btn"
                onClick={onClose}
              ></Button>
            </div>
            <div className="show_event_body">
              {selectedMenu === "EVENTMETRICS" ? (
                <EventMetrics />
              ) : (
                <Attendance />
              )}
            </div>
          </div>
        </div>
      </Drawer>
    </div>
  );
};

export default Event;
