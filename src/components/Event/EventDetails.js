import React, { Fragment } from "react";
import { Button } from "antd";
import {
  AiOutlineClose,
  AiOutlineExclamationCircle,
  AiOutlinePlus,
} from "react-icons/ai";
import { FaUserCircle } from "react-icons/fa";
import eventBg from "../../images/event_bg.png";

const EventDetails = () => {
  return (
    <Fragment>
      <p className="show_extra_big_text show_margin_bottom_3 show_600_weight">
        East frica young techies 2020
      </p>

      <img className="show_margin_bottom_2" src={eventBg} alt="" />

      <p className="show_margin_bottom_2 show_uppercased_text show_600_weight">
        DESCRIPTION
      </p>

      <p className="show_mute_text show_margin_bottom_2">
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Reiciendis,
        ullam. Quibusdam molestiae eligendi maxime. Ex omnis sapiente eaque ipsa
        quos aut reiciendis nemo non fugit dignissimos, at officia quas
        voluptatibus.
      </p>

      <p className="show_margin_bottom_2 show_uppercased_text show_600_weight">
        DETAILS
      </p>

      <div className="show_event_details_wrapper show_margin_bottom_2">
        <div className="show_event_details show_margin_bottom_1">
          <span>Status</span>
          <span className="show_mute_text">ACTIVE</span>
        </div>
        <div className="show_event_details show_margin_bottom_1">
          <span>Category</span>
          <span className="show_mute_text">ICT</span>
        </div>
        <div className="show_event_details show_margin_bottom_1">
          <span>Event type</span>
          <span className="show_mute_text">Summit</span>
        </div>
        <div className="show_event_details show_margin_bottom_1">
          <span>Data & Time</span>
          <span className="show_mute_text"> 14 Feb 2020 - 6:00 PM</span>
        </div>
        <div className="show_event_details show_margin_bottom_1">
          <span>Event venue</span>
          <a
            href="https://www.serenahotels.com/serenakigali/en/default.html"
            target="_blank"
            rel="noreferrer"
          >
            Kigali Serena Hotel
          </a>
        </div>
      </div>

      <p className="show_margin_bottom_2 show_uppercased_text show_600_weight">
        EVENT
      </p>

      <div className="show_event_details_wrapper show_margin_bottom_2">
        <div className="show_event_details show_margin_bottom_1">
          <span>Event Hashtag</span>
          <span className="show_mute_text">#EACDH2020</span>
        </div>
      </div>

      <div className="show_event_guest_wrapper">
        <div className="show_event_guest">
          <p className="show_uppercased_text show_600_weight">
            GUESTS OF HONOR
          </p>
          <div className="guest_profile">
            <FaUserCircle className="profile_pic" />
            <p>
              Yves Honore - <span className="show_mute_text">UX/UX Lead</span>
            </p>
          </div>
        </div>
        <div className="show_event_guest">
          <Button
            type="link"
            icon={<AiOutlinePlus />}
            className="show_link_btn show_link_btn_primary show_red_btn"
          >
            Add guest
          </Button>
          <div className="show_event_guest_icons">
            <AiOutlineExclamationCircle className="show_event_guest_icon" />
            <AiOutlineClose className="show_event_guest_icon" />
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default EventDetails;
