import React, { useState } from "react";
import {
  Button,
  Modal,
  Form,
  Input,
  Select,
  Upload,
  DatePicker,
  TimePicker,
} from "antd";
import ImgCrop from "antd-img-crop";
import { AiOutlineClose } from "react-icons/ai";
import { BiTimeFive } from "react-icons/bi";
import { BiImageAdd } from "react-icons/bi";
import { requiredRules } from "../Validations";

const AddEvent = ({ isModalVisible, handleOk, handleCancel }) => {
  const [fileList, setFileList] = useState([{}]);
  const [loading, setLoading] = useState(false);
  const [image, setImage] = useState("");

  const onFinish = (values) => {
    console.log("Success:", values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };
  const onUploadChange = async ({ fileList: newFileList }) => {
    setFileList(newFileList);
    const data = new FormData();
    data.append("file", fileList[0]?.originFileObj);
    data.append("upload_preset", "show_app_images");
    setLoading(true);
    const res = await fetch(
      "	https://api.cloudinary.com/v1_1/dsq8qsg1a/image/upload",
      {
        method: "POST",
        body: data,
      }
    );

    const theFile = await res.json();
    setImage(theFile.secure_url);
    setLoading(false);
  };

  const onPreview = async (file) => {
    let src = file.url;
    if (!src) {
      src = await new Promise((resolve) => {
        const reader = new FileReader();
        reader.readAsDataURL(file.originFileObj);
        reader.onload = () => resolve(reader.result);
      });
    }
    const image = new Image();
    image.src = src;
    const imgWindow = window.open(src);
    imgWindow.document.write(image.outerHTML);
  };

  return (
    <Modal
      title=""
      visible={isModalVisible}
      onOk={handleOk}
      onCancel={handleCancel}
      width={800}
      footer={false}
      closable={false}
    >
      <div className="show_modal">
        <div className="show_modal_header">
          <div className="header_content">
            <h6 className="show_big_text show_600_weight">Create new event</h6>
            <AiOutlineClose
              className="show_cursored show_text_18"
              onClick={handleCancel}
            />
          </div>
        </div>
        <Form
          name="addEvent"
          onFinish={onFinish}
          onFinishFailed={onFinishFailed}
          layout="vertical"
        >
          <div className="show_modal_body">
            <div className="body_content">
              <div className="top show_mb_64">
                {loading ? <p>loading</p> : <img src={image} alt="" />}
                <div className="image_upload">
                  <ImgCrop rotate>
                    <Upload
                      listType="picture-card"
                      fileList={fileList}
                      onChange={onUploadChange}
                      onPreview={onPreview}
                      className="show_uploader"
                    >
                      <BiImageAdd />
                      Upload image
                    </Upload>
                  </ImgCrop>
                </div>
                <div className="top_form">
                  <Form.Item
                    label="Event name"
                    name="eventName"
                    rules={requiredRules}
                  >
                    <Input className="show_input" />
                  </Form.Item>
                  <Form.Item label="Event category">
                    <Select className="show_input" defaultValue="One">
                      <Select.Option value="One">One</Select.Option>
                      <Select.Option value="Two">Two</Select.Option>
                      <Select.Option value="Three">Three</Select.Option>
                    </Select>
                  </Form.Item>
                  <Form.Item label="Event category">
                    <Select className="show_input" defaultValue="One">
                      <Select.Option value="One">One</Select.Option>
                      <Select.Option value="Two">Two</Select.Option>
                      <Select.Option value="Three">Three</Select.Option>
                    </Select>
                  </Form.Item>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-12 col-md-12 col-12">
                  <Form.Item
                    label="Description"
                    name="Description"
                    rules={requiredRules}
                  >
                    <Input.TextArea className="show_input" />
                  </Form.Item>
                </div>
                <div className="col-lg-4 col-md-4 col-12">
                  <Form.Item label="Date" name="Date" rules={requiredRules}>
                    <DatePicker placeholder="mm/dd/yyy" />
                  </Form.Item>
                </div>
                <div className="col-lg-4 col-md-4 col-12">
                  <Form.Item
                    label="Start time"
                    name="startTime"
                    rules={requiredRules}
                  >
                    <TimePicker
                      placeholder="--:-- --"
                      suffixIcon={BiTimeFive}
                    />
                  </Form.Item>
                </div>
                <div className="col-lg-4 col-md-4 col-12">
                  <Form.Item
                    label="End time"
                    name="endTime"
                    rules={requiredRules}
                  >
                    <TimePicker
                      placeholder="--:-- --"
                      suffixIcon={BiTimeFive}
                    />
                  </Form.Item>
                </div>
                <div className="col-lg-12 col-md-12 col-12">
                  <Form.Item label="Venue" name="Venue" rules={requiredRules}>
                    <Input className="show_input" />
                  </Form.Item>
                </div>
              </div>
            </div>
          </div>
          <div className="show_modal_footer">
            <Form.Item>
              <Button
                className="show_btn show_secondary_btn capitalize_btn"
                type="primary"
                htmlType="submit"
              >
                Save event
              </Button>
            </Form.Item>
            <Form.Item>
              <Button
                className="show_btn show_primary_btn capitalize_btn"
                type="primary"
                htmlType="submit"
              >
                Save and publish
              </Button>
            </Form.Item>
          </div>
        </Form>
      </div>
    </Modal>
  );
};

export default AddEvent;
