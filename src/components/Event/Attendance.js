import React from "react";
import { Input } from "antd";
import AttendeesTable from "../Tables/AttendeesTable";

const { Search } = Input;

const Attendance = () => {
  const onSearch = (value) => console.log(value);
  return (
    <div className="show_attendence">
      <div className="show_attendence_header">
        <div className="total">
          <h6 className="show_big_text show_600_weight">140 Attending</h6>
        </div>
        <div>
          <p className="show_cursored show_600_weight">Filter</p>
        </div>
        <div>
          <p className="show_cursored show_600_weight">Sort</p>
        </div>
        <div>
          <Search
            className="show_input"
            placeholder="Search"
            onSearch={onSearch}
          />
        </div>
      </div>
      <div className="show_attendence_body show_pt_1">
        <AttendeesTable />
      </div>
    </div>
  );
};

export default Attendance;
