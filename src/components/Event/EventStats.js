import React from "react";

const EventStats = () => {
  return (
    <div className="col-md-4 col-lg-4 col-12">
      <div className="show_number_card">
        <span className="show_margin_bottom_1 show_big_text show_red_text">
          2,100
        </span>
        <span className="show_small_text">New users</span>
      </div>
    </div>
  );
};

export default EventStats;
