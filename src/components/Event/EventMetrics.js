import React from "react";
import EventStats from "./EventStats";
import EventProgress from "./EventProgress";

const EventMetrics = () => {
  return (
    <div className="show_metrics show_pt_1">
      <p className="show_big_text show_600_weight show_margin_bottom_1">
        Stats
      </p>
      <div className="row">
        {[...Array(3)].map((_, index) => (
          <EventStats key={index} />
        ))}
      </div>

      <p className="show_big_text show_600_weight show_margin_bottom_1">
        Attendees interests
      </p>

      <div className="show_stats_progress show_margin_bottom_1">
        {[...Array(5)].map((_, index) => (
          <EventProgress key={index} />
        ))}
      </div>
    </div>
  );
};

export default EventMetrics;
