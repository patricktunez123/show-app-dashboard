import Skeleton, { SkeletonTheme } from "react-loading-skeleton";

const TableLoader = () => {
  return (
    <>
      {[...Array(7)].map((_, index) => (
        <div key={index} className="col-lg-12 col-md-12 col-12">
          <SkeletonTheme color="#fff" highlightColor="#f7fafd">
            <Skeleton width="100%" height="4rem" />
          </SkeletonTheme>
        </div>
      ))}
    </>
  );
};

export default TableLoader;
