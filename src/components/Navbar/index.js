import React, { useState } from "react";
import { useHistory } from "react-router-dom";
import { AiOutlineQuestionCircle } from "react-icons/ai";
import { FaUserCircle } from "react-icons/fa";
import { IoMdNotifications } from "react-icons/io";
import "./Navbar.scss";
import Notifications from "../Notifications";
import { handleTitle } from "../../helpers/handleTitle";

const Navbar = () => {
  const history = useHistory();
  const [visible, setVisible] = useState(false);

  const showDrawer = () => {
    setVisible(true);
  };

  const onClose = () => {
    setVisible(false);
  };

  return (
    <div className="show_navbar">
      <h6 className="show_big_text show_600_weight">
        {history.location.pathname && history.location.pathname
          ? handleTitle(history.location.pathname)
          : "Show Dashboard"}
      </h6>

      <div className="show_navbar_right">
        <AiOutlineQuestionCircle className="show_cursored nav_icon" />
        <div className="profile_info show_cursored">
          <FaUserCircle className="profile_icon" />
          <span className="show_black_text">Yves Honore</span>
        </div>
        <IoMdNotifications
          onClick={showDrawer}
          className="show_cursored nav_icon"
        />
        <Notifications onClose={onClose} visible={visible} />
      </div>
    </div>
  );
};

export default Navbar;
