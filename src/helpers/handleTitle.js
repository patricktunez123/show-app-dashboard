export const handleTitle = (string) => {
  return (
    string
      .replace(/^[\\/]+|[\\/]+$/g, "")
      .charAt(0)
      .toUpperCase() + string.slice(2)
  );
};
