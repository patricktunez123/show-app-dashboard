import React, { Fragment, Suspense } from "react";
import { Route, Switch } from "react-router-dom";
import { routes } from "../../config/route-config";
import LazyLoadingLoader from "../../components/Loaders/LazyLoadingLoader";
import Layout from "../../components/Layout";

const Dashboard = React.lazy(() => import("../../views/dashboard"));
const Profile = React.lazy(() => import("../../views/profile"));
const Settings = React.lazy(() => import("../../views/settings"));

const PrivateRoutes = () => {
  return (
    <Fragment>
      <Suspense
        fallback={
          <Layout>
            <LazyLoadingLoader />
          </Layout>
        }
      >
        <Switch>
          <Route path={routes.dashboard.url} exact>
            <Layout>
              <Dashboard />
            </Layout>
          </Route>
          <Route path={routes.profile.url} exact>
            <Layout>
              <Profile />
            </Layout>
          </Route>
          <Route path={routes.settings.url} exact>
            <Layout>
              <Settings />
            </Layout>
          </Route>
        </Switch>
      </Suspense>
    </Fragment>
  );
};

export default PrivateRoutes;
