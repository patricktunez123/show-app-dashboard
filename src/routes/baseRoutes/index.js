import React, { Fragment, Suspense } from "react";
import { Route, Switch } from "react-router-dom";
import { routes } from "../../config/route-config";
import LazyLoadingLoader from "../../components/Loaders/LazyLoadingLoader";
import Layout from "../../components/Layout";
import Login from "../../views/login";
import Register from "../../views/register";
import ForgetPassword from "../../views/forgetPassword";
import ResetPassword from "../../views/ResetPassword";
import Error404 from "../../views/error404";

const PrivateRoutes = React.lazy(() => import("../privateRoutes"));

const BaseRoutes = () => {
  return (
    <Fragment>
      <Suspense
        fallback={
          <Layout>
            <LazyLoadingLoader />
          </Layout>
        }
      >
        <Switch>
          <Route path={routes.login.url} exact>
            <Login />
          </Route>

          <Route path={routes.register.url} exact>
            <Register />
          </Route>

          <Route path={routes.forgetPassword.url} exact>
            <ForgetPassword />
          </Route>

          <Route path={routes.resetPassword.url} exact>
            <ResetPassword />
          </Route>

          <Route component={PrivateRoutes} />
          <Route>
            <Error404 />
          </Route>
        </Switch>
      </Suspense>
    </Fragment>
  );
};

export default BaseRoutes;
