import React from "react";
import { Link, useHistory } from "react-router-dom";
import { Form, Input, Button } from "antd";
import { routes } from "../../config/route-config";
import red_icon from "../../images/red_icon.png";
import "./Login.scss";
import { emailRules, passwordRules } from "../../components/Validations";

const Login = () => {
  const history = useHistory();
  const onFinish = (values) => {
    console.log("Success:", values);
    history.push(routes.dashboard.url);
  };

  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className="show_auth_page">
      <div className="auth_page_content">
        <div className="auth_page_container">
          <div className="show_auth_loading" />
          <div className="show_icon show_mb_32">
            <img src={red_icon} alt="" />
          </div>
          <p className="show_pb_1 show_big_text show_600_weight">
            Organizer login
          </p>
          <p className="show_mute_text show_mb_64">
            Use your company email to login.
          </p>

          <Form
            name="loginForm"
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            layout="vertical"
          >
            <Form.Item label="Email" name="email" rules={emailRules}>
              <Input className="show_input" placeholder="Email" />
            </Form.Item>

            <Form.Item label="Password" name="password" rules={passwordRules}>
              <Input.Password
                className="show_input"
                placeholder="Enter password"
              />
            </Form.Item>

            <Form.Item className="show_auth_btn_container">
              <Button
                className="show_btn show_primary_btn show_auth_btn"
                type="primary"
                htmlType="submit"
              >
                Login
              </Button>
            </Form.Item>
          </Form>

          <p className="text-center">
            <span className="show_mute_text">Forget password?</span>{" "}
            <Link className="swow_auth_link" to={routes.forgetPassword.url}>
              Reset
            </Link>
          </p>
        </div>
      </div>

      <div className="auth_page_cliped"></div>
    </div>
  );
};

export default Login;
