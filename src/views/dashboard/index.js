import React, { useState } from "react";
import { Button } from "antd";
import eventImage from "../../images/newevent.png";
import "./Dashboard.scss";
import Event from "../../components/Event";
import AddEvent from "../../components/Event/AddEvent";

const Dashboard = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [selectedMenu, setSelectedMenu] = useState("EVENTMETRICS");

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };
  const showModal = () => {
    setIsModalVisible(true);
  };

  const onEventMetrics = () => {
    setSelectedMenu("EVENTMETRICS");
  };

  const onEventAttendees = () => {
    setSelectedMenu("ATTENDEES");
  };

  return (
    <div className="show_content">
      <div className="show_content_container show_p_2">
        <div className="show_banner">
          <div className="show_banner_left">
            <img src={eventImage} alt="" />
          </div>
          <div className="show_banner_right">
            <h6 className="show_margin_bottom_2 show_extra_big_text show_white_text">
              Have an event in mind?
            </h6>
            <Button className="show_btn show_primary_btn" onClick={showModal}>
              Create event
            </Button>
            <AddEvent
              isModalVisible={isModalVisible}
              handleOk={handleOk}
              handleCancel={handleCancel}
            />
          </div>
        </div>

        <h6 className="show_margin_bottom_3 show_margin_3_top show_big_text show_600_weight">
          My recent events
        </h6>

        <div className="show_events">
          <div className="row">
            {[...Array(4)].map((_, index) => (
              <Event
                key={index}
                showModal={showModal}
                onEventMetrics={onEventMetrics}
                onEventAttendees={onEventAttendees}
                selectedMenu={selectedMenu}
              />
            ))}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Dashboard;
