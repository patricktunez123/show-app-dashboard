export const routes = {
  login: {
    name: "Login",
    url: "/",
  },
  register: {
    name: "Register",
    url: "/create-account",
  },
  forgetPassword: {
    name: "ForgetPassword",
    url: "/forget-password",
  },
  resetPassword: {
    name: "ResetPassword",
    url: "/reset-password",
  },
  dashboard: {
    name: "Dashboard",
    url: "/dashboard",
  },
  profile: {
    name: "Profile",
    url: "/profile",
  },
  settings: {
    name: "Settings",
    url: "/settings",
  },
};
