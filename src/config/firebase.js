import firebase from "firebase";

const firebaseConfig = {};

let app = null;

firebase.apps.length === 0
  ? (app = firebase.initializeApp(firebaseConfig))
  : (app = firebase.app());

export const db = app.firestore();
export const auth = app.auth();
export const storage = app.storage();
